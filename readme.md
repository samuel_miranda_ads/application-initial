# Laravel ACL 

ACL é uma arquitetura de autenticação e autorização baseada em Roles (Papéis) e Permissions (Permissões)
 cada usuário no sistema está associado a determinadas roles que estão associadas a várias permissões.
 

#Instruções de Instalação:

1 - Clone este projeto normalmente para uma pasta de preferência

2 - Execute o comando "composer install" na pasta do projeto

3 - configure o arquivo .env com as preferências

4 - Após isso execute o comando "php artisan key:generate"

5 - Execute o comando "php artisan migrate"

6 - Execute o comando "php artisan db:seed"