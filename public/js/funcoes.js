function abrirCarrega(velocidade) {
	if (velocidade && velocidade.trim() != "") {
		jQuery(".preloader").fadeIn(velocidade);
	} else {
		jQuery(".preloader").fadeIn();
	}

//	jQuery("body").css('overflow', 'hidden');
}

function fecharCarrega(velocidade) {
	setTimeout(
		function() {
//			jQuery("body").removeAttr('style');

			if (velocidade && velocidade.trim() != "") {
				jQuery(".preloader").fadeOut(velocidade);
			} else {
				jQuery(".preloader").fadeOut();
			}
		}, 500
	);
}

function abrirTelaPopup(opcoes) {
	if (opcoes && ((opcoes.url && opcoes.url.trim() != "") || (opcoes.html && opcoes.html.trim() != ""))) {
		abrirCarrega('fast');

		if (!opcoes.titulo && opcoes.titulo.trim() == "") {
			opcoes.titulo = "Novo";
		}

		var dados = "";

		if (opcoes.dados) {
			jQuery.each(opcoes.dados, function(idx, el) {
				//ExpressÃ£o regular para identificar uma DATA
				if (opcoes.titulofiltro  && opcoes.titulofiltro != "graficosPC" && el.match(/^\b(\d+\/\d+\/\d+)\b$/g) != null) {
					if (el.length == 10) {
						el = el.substring(6, 10) + el.substring(3, 5) + el.substring(0, 2);
					} else if (el.length == 8) {
						el = "20" + el.substring(6, 8) + el.substring(3, 5) + el.substring(0, 2);
					}

					opcoes.dados[idx+""] = el;
				}
			});

			dados = '&dados=' + JSON.stringify(opcoes.dados);
		}

		var espaco = 105;
		var css = "";
		if (opcoes.css && opcoes.css.trim() != "") {
			css = opcoes.css;
		}

		var telaPopup = jQuery(
			"<div class='fundoDlg' id='telaPopup'>"
				+"<div class='telaPopupCorpo' style='" + css + "'>"
					+"<fielset class='telaPopupFielset'>"
						+"<legend>" + opcoes.titulo + "<i class='fa fa-times' id='telaPopupFechar'></i><i aria-hidden='true' class='fa fa-expand' title='Maximizar'></i><i aria-hidden='true' class='fa fa-compress' title='Minimizar'></i></legend>"
						+"<div class='telaPopupContent carregando'>"
						+"</div>"
						+"<input class='btn' type='button' id='btnFechar' value='Fechar'/>"
					+"</fielset>"
				+"</div>"
			+"</div>"
		);

		var telaPopupFechar = jQuery('#telaPopupFechar', telaPopup);
		telaPopupFechar.click(
			function() {
				fecharCarrega();
				telaPopup.css("opacity", "0");
				jQuery("body").removeAttr('style');
				setTimeout(
					function() {
						telaPopup.remove();
					}, 1000
				);
			}
		);

		var btnFechar = jQuery('#btnFechar', telaPopup);
		btnFechar.click(
			function() {
				telaPopupFechar.click();
			}
		);

		var btnMaximizar = jQuery('.fa-expand', telaPopup);
		btnMaximizar.click(
			function() {
				var corpo = jQuery('.telaPopupCorpo', telaPopup);
				corpo.addClass('maximizado');
				setTimeout(
					function() {
						jQuery(window).resize();
					}, 200
				);
			}
		);

		var btnMinimizar = jQuery('.fa-compress', telaPopup);
		btnMinimizar.click(
			function() {
				var corpo = jQuery('.telaPopupCorpo', telaPopup);
				corpo.removeClass('maximizado');

				jQuery(window).resize();
				setTimeout(
					function() {
						jQuery(window).resize();
					}, 100
				);
			}
		);

		if(opcoes && opcoes.salvar) {
			var labelSalvar = opcoes.labelSalvar ? opcoes.labelSalvar.trim() : "Salvar";
			try {
				var btnSalvar = jQuery("<input class='btn' type='button' id='btnSalvar' value='" + labelSalvar + "' style='background-color: rgb(75, 75, 75); right: " + espaco + "px;'/>");

				btnSalvar.click(
					function(event) {
						if(opcoes && opcoes.antesSalvar) {
							opcoes.antesSalvar();
						}

						var resp = false;

						jQuery.when(resp = opcoes.salvar()).done(
							function() {
								if (resp) {
									if (!isNaN(resp)) {
										console.log("Inserido com sucesso.");
									} else {
										console.log("Erro ao salvar.");
									}

								}
							}
						);
					}
				);

				espaco += 87;
				btnSalvar.insertBefore(btnFechar);
			} catch(err) {

				console.log("Verifique a função salvar.");
				fecharCarrega();
			}
		}

		if(opcoes && opcoes.editar) {
			var labelEditar = opcoes.labelEditar ? opcoes.labelEditar.trim() : "Salvar";
			try {
				var btnEditar= jQuery("<input class='btn' type='button' id='btnEditar' value='" + labelEditar + "' style='background-color: rgb(75, 75, 75); right: " + (espaco+1.75) + "px;'/>");

				btnEditar.click(
					function(event) {
						if(opcoes && opcoes.antesEditar) {
							opcoes.antesEditar();
						}

						var resp = false;

						jQuery.when(resp = opcoes.editar()).done(
							function() {
								if (resp) {
									if (resp == true) {
										console.log("Editado com sucesso.");
									} else {
										console.log("Erro ao editar.");
									}

								}
							}
						);
					}
				);

				espaco += 87;
				btnEditar.insertBefore(btnFechar);
			} catch(err) {

				console.log("Verifique a função editar.");
				fecharCarrega();
			}
		}

		if(opcoes && opcoes.excluir) {
			var labelExluir = opcoes.labelExluir ? opcoes.labelExluir.trim() : "Excluir";
			try {
				var btnExcluir = jQuery("<input class='btn' type='button' id='btnExcluir' value='" + labelExluir + "' style='background-color: #EC3237; right: " + espaco + "px;'/>");

				btnExcluir.click(
					function(event) {
						if(confirm("Deseja realmente excluir esse item?")) {
							if(opcoes && opcoes.antesExcluir) {
								opcoes.antesExcluir();
							}

							var resp = false;

							jQuery.when(resp = opcoes.excluir()).done(
								function() {
									if (resp) {
										btnFechar.click();

										if(opcoes && opcoes.depoisExcluir) {
											opcoes.depoisExcluir();
										}
									}
								}
							);
						}
					}
				);

				espaco += 87;
				btnExcluir.insertBefore(btnFechar);
			} catch(err) {

				console.log("Verifique a função excluir.");
				fecharCarrega();
			}
		}

		if(opcoes && opcoes.outroBtn) {
			var labelOutroBtn = opcoes.labelOutroBtn ? opcoes.labelOutroBtn.trim() : "Outro";
			try {
				var btnOutroBtn = jQuery("<input class='btn' type='button' id='btnOutroBtn' value='" + labelOutroBtn + "' style='background-color: rgb(75, 75, 75); right: " + espaco + "px;'/>");

				btnOutroBtn.click(
					function(event) {
						opcoes.outroBtn();
					}
				);

				espaco += 107;
				btnOutroBtn.insertBefore(btnFechar);
			} catch(err) {

				console.log("Verifique a função outroBtn.");
				fecharCarrega();
			}
		}

		jQuery("body").css('overflow', 'hidden').append(telaPopup);


		jQuery(window).resize(
			function(event) {
				var alturaBrowser    = jQuery(window).height();   // altura do browser
				var alturaPopup      = alturaBrowser - 50;       // altura do popup
				var telaPopupContent = jQuery(".telaPopupContent", telaPopup);
				var telaPopupCorpo   = jQuery(".telaPopupCorpo", telaPopup);

				if (alturaPopup < 250) {
					alturaPopup = alturaBrowser;
				}

				telaPopupCorpo.css({
					"max-height" : alturaPopup + "px"
				});

				if (telaPopupCorpo.hasClass('maximizado')) {

					telaPopupContent.css({
						"max-height" : (alturaBrowser - 64) + "px"
					});

				} else {
					telaPopupContent.css({
						"max-height" : (alturaPopup - 38) + "px"
					});
				}

				setTimeout(
					function() {
						var telaPopupCorpoAlt = telaPopupCorpo.height() + 18;
						telaPopupCorpo.css({
							"margin-top" : "-" + (telaPopupCorpoAlt / 2) + "px"
						});
					}, 250
				);
			}
		);

		telaPopup.css("opacity", "1");
		var def = new jQuery.Deferred();

		if (opcoes.url && opcoes.url.trim() != "") {
			jQuery.ajax({
				url : opcoes.url, //+'&titulo='+opcoes.titulo + dados,
				type: 'GET'
			}).done(
				function(retorno) {
					telaPopup.find(".carregando").html(retorno).removeClass('carregando');
				}
			).fail(
				function() {
					alert("Erro ao abrir a página");
//					location.reload();
				}
			).always(
				function() {
					jQuery(window).resize();
					setTimeout(
						function() {
							jQuery(window).resize();
							def.resolve();
						}, 1000
					);
				}
			);
		}

		if (opcoes.html && opcoes.html.trim() != "") {
			telaPopup.find(".carregando").html(opcoes.html).removeClass('carregando');
			def.resolve();
		}

		jQuery.when(def).done(
			function() {
				if(opcoes && opcoes.onload) {
					opcoes.onload();
				}

				if(opcoes && opcoes.ready) {
					abrirCarrega();
					jQuery.when(opcoes.ready()).done(
						function() {
							fecharCarrega();
						}
					);
				}

				telaPopup.css("opacity", "1");
				setTimeout(
					function() {
						jQuery(window).resize();
						fecharCarrega();
					}, 100
				);
			}
		);

	}
}

jQuery.fn.tabelaSimples = function(opcoes) {
	this.each(
		function() {
			var tabHtml = this;
			var tabela  = jQuery(this);

			if (!tabela.hasClass("tabela")) {
				tabela.addClass("tabela");
			}

			// Classes padroes
			tabela.addClass("responsive");
			tabela.addClass("table");

			tabela.DataTable({
				"language": {
					"sEmptyTable"       : "Nenhum registro encontrado",
					"sInfo"             : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
					"sInfoEmpty"        : "Mostrando 0 até 0 de 0 registros",
					"sInfoFiltered"     : "(Filtrados de _MAX_ registros)",
					"sInfoPostFix"      : "",
					"sInfoThousands"    : ".",
					"sLengthMenu"       : "_MENU_",
					"sLoadingRecords"   : "Carregando...",
					"sProcessing"       : "Processando...",
					"sZeroRecords"      : "Nenhum registro encontrado",
					"sSearch"           : "",
					"searchPlaceholder" : "Pesquisar",
					"oPaginate"       : {
						"sNext"     : "&raquo;",
						"sPrevious" : "&laquo;",
						"sFirst"    : "Primeiro",
						"sLast"     : "Ultimo"
					},
					"oAria": {
						"sSortAscending" : ": Ordenar colunas de forma ascendente",
						"sSortDescending": ": Ordenar colunas de forma descendente"
					}
				},
				"columnDefs" : (opcoes && opcoes.columnDefs) ? opcoes.columnDefs : "",
				"data"       : (opcoes && opcoes.data)       ? opcoes.data       : "",
				"order"      : (opcoes && opcoes.order)      ? opcoes.order      : "",
				"responsive" : true,
				"lengthMenu" : [
					[10,                25,                50,                100,                -1],
					['Exibir 10 itens', 'Exibir 25 itens', 'Exibir 50 itens', 'Exibir 100 itens', "Exibir todos"]
				]
			});

			jQuery.extend(
				jQuery.fn.dataTableExt.oSort, {
					"data-hora-pre" : function (data) {

						// RETIRA OS ESPAÇOS
						data = data.replace(/ /g, "");

						if (!data) {
							return 0;
						}

						// QUEBRA A DATA EM UM ARRAY COM DIA, MÊS, ANO E HORA
						var dataArray = data.split(/[\.\-\/]/);

						// TRATA OS VALORES DE DIA, MÊS, ANO E HORA
						var dia  = dataArray[0] ? (dataArray[0].length == 1) ? 0 + dataArray[0] : dataArray[0] : 0;
						var mes  = dataArray[1] ? (dataArray[1].length == 1) ? 0 + dataArray[1] : dataArray[1] : 0;
						var ano  = dataArray[2] ? dataArray[2] : 0;
						var hora = dataArray[3] ? dataArray[3].split(":")[0] + dataArray[3].split(":")[1] : '';

						var retorno = ((ano + mes + dia + hora) * 1);

						return retorno;
					},
					"data-hora-asc" : function (a, b) {
						return ((a < b) ? -1 : ((a > b) ? 1 : 0));
					},
					"data-hora-desc" : function (a, b) {
						return ((a < b) ? 1 : ((a > b) ? -1 : 0));
					}
				}
			);

			tabela.on( 'click', 'tr', function () {
				if ( $(this).hasClass('selected') ) {
					$(this).removeClass('selected');
				}
				else {
					$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});

		}
	);
}

function strToFloat(umValor) {

	umValor = umValor.replace(/\./g, "");
	umValor = umValor.replace(/,/g, ".");
	umValor = parseFloat(umValor);

	return umValor;
}

function removeCharEspecial(valor){
	var novoValor = valor.replace(/@/g, "");
			novoValor = novoValor.replace(/#/g, "");
	return novoValor;
}

function numberFormatAjax(value, size) {
	var def = new jQuery.Deferred();
	var str = "";

	$.ajax(
		{
			url   : '/number_format',
			type  : 'POST',
			async : false,
			data  : {
				value : value,
				size  : size
			}
		}
	).done(
		function(string) {
			str = string;
			def.resolve();
		}
	).fail(
		function() {
			str = value;
		}
	);

	return str;
}

jQuery(document).ready(
	function($) {
		// Menus
		$("#menuOpenClose").click(
			function() {
				var esse       = $(this);
				var i          = esse.find("i");
				var body       = $("body");
				var hiddenXs   = $("span.hidden-xs");
				var sideBar    = $(".sidebar");
				var sideBarNav = $(".sidebar-nav", sideBar);

				if (i.attr('class') == "icon-arrow-left-circle ti-menu") {
					i.removeAttr('class').addClass('ti-menu');

					body.addClass('content-wrapper');
					hiddenXs.css('display', 'none');
					sideBar.css('overflow', 'visible');
					sideBarNav.css({
						'overflow'  : 'visible'
					});

				} else {
					i.removeAttr('class').addClass('icon-arrow-left-circle ti-menu');

					body.removeClass('content-wrapper');
					hiddenXs.css('display', 'inline');
					sideBar.css('overflow', 'visible');
					sideBarNav.css('overflow', 'hidden');
				}
			}
		);

		$(".submenu").click(
			function() {
				var esse   = $(this);
				var parent = esse.parent();
				var nav    = parent.find("ul.nav");

				if(parent.hasClass('active')) {
					parent.removeClass('active');
					nav.removeClass('in').attr('aria-expanded', 'false');
				} else {
					parent.addClass('active');
					nav.addClass('in').attr('aria-expanded', 'true');
				}
			}
		);

		$(".userOpc").click(
			function() {
				var esse = $(this);
				var parent = esse.parent();

				if (parent.hasClass('open')) {
					parent.removeClass('open');
					esse.attr('aria-expanded', 'false');
					esse.attr('aria-expanded', 'true');
				} else {
					parent.addClass('open');
					esse.attr('aria-expanded', 'true');
				}

			}
		);

		// Retirando o preloader
		fecharCarrega();
		$(".sidebar").css('overflow', 'visible');

		var url = window.location;

		var element = $('ul.nav#side-menu a').filter(function () {
			return ((this.href == url && this.href != "") || (url.href.indexOf(this.href) == 0 && this.href != ""));
		}).addClass('active').parent().parent().addClass('in').parent();

		if (element.is('li')) {
			element.addClass('active');
		}

		if (url.pathname == "/") {
			$("a[href='/home']").addClass('active');
		}
	}
);
