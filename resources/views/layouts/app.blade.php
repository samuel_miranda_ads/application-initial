<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" sizes="16x16" href="/plugins/images/favicon.png">

    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- vector map CSS -->
    <link href="/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="/plugins/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme"  rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>


<body>
<!-- Preloader -->
<div class="preloader">
  <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
  </svg>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="index.html"><b><img src="/plugins/images/eliteadmin-logo.png" alt="home" /></b><span class="hidden-xs"><img src="/plugins/images/eliteadmin-text.png" alt="home" /></span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">

        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"><b class="hidden-xs">{{Auth::user()->name}}</b> </a>
          <ul class="dropdown-menu dropdown-user scale-up">
            <li><a href="#"><i class="ti-user"></i> Perfil </a></li>
            <li role="separator" class="divider"></li>
            <li class=''>
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i>
                    Sair
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>
  <!-- Left navbar-header -->
  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
       <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <!-- input-group -->
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li class="nav-small-cap m-t-10">--- Main Menu</li>
        <li> <a href="index.html" class="waves-effect active"><i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw" ></i> <span class="hide-menu"> Dashboard <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">4</span></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="index.html">Demographical</a> </li>
            <li> <a href="index2.html">Minimalistic</a> </li>
            <li> <a href="index3.html">Analitical</a> </li>
            <li> <a href="index4.html">Simpler</a> </li>
          </ul>
        </li>
        <li><a href="inbox.html" class="waves-effect"><i class="zmdi zmdi-apps zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Apps <span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">New</span></span></a>
          <ul class="nav nav-second-level">
            <li><a href="chat.html">Chat-message</a></li>
            <li><a href="javascript:void(0)" class="waves-effect">Inbox<span class="fa arrow"></span></a>
              <ul class="nav nav-third-level">
                <li> <a href="inbox.html">Mail box</a></li>
                <li> <a href="inbox-detail.html">Inbox detail</a></li>
                <li> <a href="compose.html">Compose mail</a></li>
              </ul>
            </li>
            <li><a href="javascript:void(0)" class="waves-effect">Contacts<span class="fa arrow"></span></a>
              <ul class="nav nav-third-level">
                <li> <a href="contact.html">Contact1</a></li>
                <li> <a href="contact2.html">Contact2</a></li>
                <li> <a href="contact-detail.html">Contact Detail</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i class="zmdi zmdi-format-color-fill zmdi-hc-fw fa-fw"></i> <span class="hide-menu">UI Elements<span class="fa arrow"></span> <span class="label label-rouded label-info pull-right">13</span> </span></a>
          <ul class="nav nav-second-level">
            <li><a href="panels-wells.html">Panels and Wells</a></li> <li><a href="panel-ui-block.html">Panels With BlockUI</a></li>
            <li><a href="buttons.html">Buttons</a></li>
            <li><a href="sweatalert.html">Sweat alert</a></li>
            <li><a href="typography.html">Typography</a></li>
            <li><a href="grid.html">Grid</a></li>
            <li><a href="tabs.html">Tabs</a></li>
<li><a href="tab-stylish.html">Stylish Tabs</a></li>
            <li><a href="modals.html">Modals</a></li>
            <li><a href="progressbars.html">Progress Bars</a></li>
            <li><a href="notification.html">Notifications</a></li>
            <li><a href="carousel.html">Carousel</a></li>
            <li><a href="list-style.html">List & Media object</a></li>
            <li><a href="user-cards.html">User Cards</a></li>
            <li><a href="timeline.html">Timeline</a></li>
            <li><a href="timeline-horizontal.html">Horizontal Timeline</a></li>
            <li><a href="nestable.html">Nesteble</a></li>
<li><a href="range-slider.html">Range Slider</a></li>
            <li><a href="bootstrap.html">Bootstrap UI</a></li>
<li><a href="tooltip-stylish.html">Stylish Tooltips</a></li>
          </ul>
        </li>

        <li> <a href="forms.html" class="waves-effect"><i class="zmdi zmdi-comment-alt-text zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Forms<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
            <li><a href="form-basic.html">Basic Forms</a></li>
<li><a href="form-layout.html">Form Layout</a></li>
            <li><a href="form-advanced.html">Form Addons</a></li>
             <li><a href="form-material-elements.html">Form Material</a></li> <li><a href="form-float-input.html">Form Float Input</a></li>
            <li><a href="form-upload.html">File Upload</a></li>
            <li><a href="form-mask.html">Form Mask</a></li>
            <li><a href="form-img-cropper.html">Image Cropping</a></li>
            <li><a href="form-validation.html">Form Validation</a></li>
            <li><a href="form-dropzone.html">File Dropzone</a></li>
            <li><a href="form-pickers.html">Form-pickers</a></li>
<li><a href="form-wizard.html">Form-wizards</a></li>
            <li><a href="form-typehead.html">Typehead</a></li>
            <li><a href="form-xeditable.html">X-editable</a></li>
<li><a href="form-summernote.html">Summernote</a></li>
            <li><a href="form-bootstrap-wysihtml5.html">Bootstrap wysihtml5</a></li>
            <li><a href="form-tinymce-wysihtml5.html">Tinymce wysihtml5</a></li>
          </ul>
        </li>

        <li> <a href="{{action('Painel\PainelController@index')}}" class="waves-effect"><i class="zmdi zmdi-comment-alt-text zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Controle de Acessos</a></li>

      </ul>
    </div>
  </div>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      @yield('content')
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2016 &copy; Elite Admin brought to you by themedesigner.in </footer>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/js/waves.js"></script>
<!-- Flot Charts JavaScript -->
<script src="/plugins/bower_components/flot/jquery.flot.js"></script>
<script src="/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<!-- google maps api -->
<script src="/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="/plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- Sparkline charts -->
<script src="/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<!-- EASY PIE CHART JS -->
<script src="/plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="/plugins/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/js/custom.min.js"></script>
<script src="/js/dashboard2.js"></script>
<!--Style Switcher -->
<script src="/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

  @yield('view.scripts')
</body>



</html>
